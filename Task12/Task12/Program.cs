﻿using System;
using Task12.CreateRobot;
using Task12.GetProduct;
using Task12.PutProduct;
using Task12.Storaged;

namespace Task12
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var storage = new Storage();
            var productStorage = new ProductStorage();

            var manager = new Manager(
                new IPutStrategy[]
                {
                    new LightRobotLoaderPut(),
                    new HeavyRobotLoaderPut()
                }, new IGetStrategy[]
                {
                    new LightRobotLoaderGet(),
                    new HeavyRobotLoaderGet()
                }, new ICreateStrategy[]
                {
                    new LightRobotLoaderCreate(),
                    new HeavyRobotLoaderCreate()
                });

            manager.CreateRobot("Light", "R2D2");
            manager.CreateRobot("Heavy", "C3PO");

            var product = new[]
            {
                new Product(40),
                new Product(90),
                new Product(5),
                new Product(55)
            };

            productStorage.Add(product);

            for (var i = 0; i < productStorage.products.Count; i++)
                manager.PutProduct(productStorage, storage);

            Console.WriteLine("Загруженный склад");
            storage.Print();

            manager.GetProduct(storage, 1);
            Console.WriteLine("Остаток на складе");
            storage.Print();
        }
    }
}