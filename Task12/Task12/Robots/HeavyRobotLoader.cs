﻿namespace Task12.Robots
{
    public class HeavyRobotLoader : RobotLoader
    {
        public HeavyRobotLoader(string robotName)
            : base(robotName)
        {
        }
    }
}