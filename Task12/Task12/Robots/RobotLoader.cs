﻿using System;

namespace Task12.Robots
{
    public abstract class RobotLoader
    {
        protected RobotLoader(string robotName)
        {
            RobotName = robotName;
        }

        public string RobotName { get; set; }

        public void Signal()
        {
            Console.WriteLine($"{RobotName}: \"К вашим услугам\"");
        }
    }
}