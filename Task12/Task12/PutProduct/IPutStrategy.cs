﻿using Task12.Storaged;

namespace Task12.PutProduct
{
    public interface IPutStrategy
    {
        public bool IsSuitable(ProductStorage productStorage);

        public void PutProducts(ProductStorage productStorage, Storage storage);
    }
}