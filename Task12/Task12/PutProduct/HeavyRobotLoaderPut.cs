﻿using Task12.Storaged;

namespace Task12.PutProduct
{
    public class HeavyRobotLoaderPut : IPutStrategy
    {
        public bool IsSuitable(ProductStorage productStorage)
        {
            return productStorage.products.Peek().Weight > 50;
        }

        public void PutProducts(ProductStorage productStorage, Storage storage)
        {
            var product = productStorage.products.Dequeue();
            product.ID = storage.storageProducts.Count;
            storage.storageProducts.Add(product);
        }
    }
}