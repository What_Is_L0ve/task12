﻿using System.Collections.Generic;

namespace Task12.Storaged
{
    public class ProductStorage
    {
        public Queue<Product> products;

        public ProductStorage()
        {
            products = new Queue<Product>();
        }

        public void Add(Product[] product)
        {
            for (var i = 0; i < product.Length; i++)
                products.Enqueue(product[i]);
        }
    }
}