﻿using System;
using System.Collections.Generic;

namespace Task12.Storaged
{
    public class Storage
    {
        public List<Product> storageProducts;

        public Storage()
        {
            storageProducts = new List<Product>();
        }

        public void Print()
        {
            for (var i = 0; i < storageProducts.Count; i++)
                Console.WriteLine(
                    $"Номер ячейки: {storageProducts[i].ID}, Вес содержимого: {storageProducts[i].Weight}");
        }
    }
}