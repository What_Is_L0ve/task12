﻿using Task12.Storaged;

namespace Task12.GetProduct
{
    public interface IGetStrategy
    {
        public bool IsSuitable(Storage storage, int ID);

        public void GetProducts(Storage storage, int ID);
    }
}