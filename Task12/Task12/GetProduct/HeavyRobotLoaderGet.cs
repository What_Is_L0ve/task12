﻿using System.Collections.Generic;
using Task12.Robots;
using Task12.Storaged;

namespace Task12.GetProduct
{
    public class HeavyRobotLoaderGet : IGetStrategy
    {
        public bool IsSuitable(Storage storage, int ID)
        {
            for (var i = 0; i < storage.storageProducts.Count; i++)
                if (storage.storageProducts[i].Weight > 50)
                    return true;

            return false;
        }

        public void GetProducts(Storage storage, int ID)
        {
            storage.storageProducts.RemoveAt(ID);
        }
    }
}