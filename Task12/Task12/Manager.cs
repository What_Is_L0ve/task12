﻿using System.Collections.Generic;
using Task12.CreateRobot;
using Task12.GetProduct;
using Task12.PutProduct;
using Task12.Robots;
using Task12.Storaged;

namespace Task12
{
    public class Manager
    {
        public static List<RobotLoader> RobotLoaders;
        private readonly ICreateStrategy[] _create;
        private readonly IPutStrategy[] _distribution;
        private readonly IGetStrategy[] _get;

        public Manager(IPutStrategy[] distribution, IGetStrategy[] get, ICreateStrategy[] create)
        {
            _distribution = distribution;
            _get = get;
            _create = create;
            RobotLoaders = new List<RobotLoader>();
        }

        public void CreateRobot(string robotModel, string robotName)
        {
            foreach (var create in _create)
                if (create.IsSuitable(robotModel))
                    create.CreateProducts(robotName, RobotLoaders);
        }

        public void PutProduct(ProductStorage productStorage, Storage storage)
        {
            for (var i = 0; i < productStorage.products.Count; i++)
                foreach (var distribution in _distribution)
                    if (distribution.IsSuitable(productStorage))
                        distribution.PutProducts(productStorage, storage);
                    else
                        break;
        }

        public void GetProduct(Storage storage, int ID)
        {
            for (var i = 0; i < storage.storageProducts.Count; i++)
                if (storage.storageProducts[i].ID == ID)
                    foreach (var get in _get)
                        if (get.IsSuitable(storage, ID))
                        {
                            get.GetProducts(storage, ID);
                            break;
                        }
                        else
                        {
                            break;
                        }
        }
    }
}