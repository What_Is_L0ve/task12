﻿using System.Collections.Generic;
using Task12.Robots;

namespace Task12.CreateRobot
{
    public interface ICreateStrategy
    {
        public bool IsSuitable(string robotModel);

        public void CreateProducts(string robotName, List<RobotLoader> list);
    }
}