﻿using System.Collections.Generic;
using Task12.Robots;

namespace Task12.CreateRobot
{
    public class LightRobotLoaderCreate : ICreateStrategy
    {
        public bool IsSuitable(string robotModel)
        {
            return robotModel == "Light";
        }

        public void CreateProducts(string robotName, List<RobotLoader> list)
        {
            var newRobot = new LightRobotLoader(robotName);
            newRobot.Signal();
            list.Add(newRobot);
        }
    }
}