﻿using System.Collections.Generic;
using Task12.Robots;

namespace Task12.CreateRobot
{
    public class HeavyRobotLoaderCreate : ICreateStrategy
    {
        public bool IsSuitable(string robotModel)
        {
            return robotModel == "Heavy";
        }

        public void CreateProducts(string robotName, List<RobotLoader> list)
        {
            var newRobot = new HeavyRobotLoader(robotName);
            newRobot.Signal();
            list.Add(newRobot);
        }
    }
}