﻿namespace Task12
{
    public class Product
    {
        public Product(int weight)
        {
            Weight = weight;
        }

        public int ID { get; set; }
        public int Weight { get; }
    }
}